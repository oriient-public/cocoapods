Pod::Spec.new do |s|
    s.name = 'IPSRemoteConfig'
    s.version = '2.2.0'
    s.summary = 'Provides access to remote configurations, manages the retry and cache logic. '
    s.homepage = 'http://www.oriient.me'

    s.author = { 'Name' => 'dev@oriient.me' }
    s.license = { :type => 'Confidential', :file => 'LICENSE' }

    s.platform = :ios
    s.source = { :http => 'https://cocoapods.oriient.me/IPSRemoteConfig/2.2.0.zip' }

    s.ios.deployment_target = '11.0'

    s.ios.vendored_frameworks = 'IPSRemoteConfig.xcframework'
    s.swift_version = '5.0'
end
