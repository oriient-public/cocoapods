Pod::Spec.new do |s|
    s.name = 'IPSFramework'
    s.version = '8.2.2'
    s.summary = 'Oriient indoor positioning framework'
    s.homepage = 'https://www.oriient.me'

    s.author = { 'Name' => 'dev@oriient.me' }
    s.license = { :type => 'Confidential', :file => 'IPSFramework.xcframework/LICENSE' }

    s.platform = :ios
    s.source = { :http => 'https://cocoapods.oriient.me/IPSFramework/8.2.2.zip' }

    s.ios.deployment_target = '13.0'

    s.ios.vendored_frameworks = 'IPSFramework.xcframework'
    s.swift_version = '5.0'
end
