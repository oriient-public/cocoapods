Pod::Spec.new do |s|
    s.name = 'OriientLocationReporter_dev'
    s.version = '0.6.1-alpha6'
    s.summary = 'OriientLocationReporter manages the reporting of the current user location updates to the remote server.'
    s.homepage = 'https://www.oriient.me'

    s.author = { 'Name' => 'dev@oriient.me' }
    s.license = { :type => 'Confidential', :file => 'LICENSE' }

    s.platform = :ios
    s.source = { :http => 'https://cocoapods.oriient.me/OriientLocationReporter_dev/0.6.1-alpha6.zip' }

    s.ios.deployment_target = '13.0'
    s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    s.dependency 'OriientCore_dev', '0.6.1-alpha6'
    s.dependency 'OriientLocationService_dev', '0.6.1-alpha6'
    s.dependency 'OriientCombine', '0.1.0'
    s.ios.vendored_frameworks = 'OriientLocationReporter.xcframework'
    s.swift_version = '5.0'
end
