Pod::Spec.new do |s|
    s.name = 'coordinatesmanager'
    s.version = '0.0.8'
    s.summary = 'Coordinates Manager to help convert to/from a custom coordinates system and Oriient Coordinate System'
    s.homepage = 'http://www.oriient.me'

    s.author = { 'Name' => 'dev@oriient.me' }
    s.license = { :type => 'Confidential', :file => 'LICENSE' }

    s.platform = :ios
    s.source = { :http => 'https://cocoapods.oriient.me/coordinatesmanager/0.0.8.zip' }

    s.ios.deployment_target = '11.0'
    s.ios.vendored_frameworks = 'coordinatesmanager.framework'
end
