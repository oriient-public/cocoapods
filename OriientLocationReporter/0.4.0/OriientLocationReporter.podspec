Pod::Spec.new do |s|
    s.name = 'OriientLocationReporter'
    s.version = '0.4.0'
    s.summary = 'OriientLocationReporter manages the reporting of the current user location updates to the remote server.'
    s.homepage = 'http://www.oriient.me'

    s.author = { 'Name' => 'dev@oriient.me' }
    s.license = { :type => 'Confidential', :file => 'LICENSE' }

    s.platform = :ios
    s.source = { :http => 'https://cocoapods.oriient.me/OriientLocationReporter/0.4.0.zip' }

    s.ios.deployment_target = '12.0'
    s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    s.dependency 'OriientCore', '0.4.0'
    s.dependency 'OriientLocationService', '0.4.0'
    s.dependency 'LightObservables', '0.2.0'
    s.ios.vendored_frameworks = 'OriientLocationReporter.xcframework'
    s.swift_version = '5.0'
end
