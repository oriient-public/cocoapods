Pod::Spec.new do |s|
    s.name = 'IPSFramework'
    s.version = '2.17.1'
    s.summary = 'Oriient indoor positioning framework'
    s.homepage = 'http://www.oriient.me'

    s.author = { 'Name' => 'dev@oriient.me' }
    s.license = { :type => 'Confidential', :file => 'LICENSE' }

    s.platform = :ios
    s.source = { :http => 'https://cocoapods.oriient.me/IPSFramework/2.17.1.zip' }

    s.ios.deployment_target = '11.0'
    s.dependency 'IPSRemoteConfig', '1.6'
    s.ios.vendored_frameworks = 'IPSFramework.framework'
end
