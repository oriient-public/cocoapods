Pod::Spec.new do |s|
    s.name = 'OriientLocationService_dev_23'
    s.version = '0.10.0'
    s.summary = 'OriientLocationService allows to actively monitor the user location.'
    s.homepage = 'https://www.oriient.me'

    s.author = { 'Name' => 'dev@oriient.me' }
    s.license = { :type => 'Confidential', :file => 'LICENSE' }

    s.platform = :ios
    s.source = { :http => 'https://cocoapods.oriient.me/OriientLocationService_dev_23/0.10.0.zip' }

    s.ios.deployment_target = '13.0'
    s.dependency 'OriientCore_dev_23', '0.10.0'
    s.dependency 'OriientCombine', '0.2.0'
    s.ios.vendored_frameworks = 'OriientLocationService.xcframework'
    s.swift_version = '5.0'
end
