Pod::Spec.new do |s|
    s.name = 'OriientLocationService'
    s.version = '0.2.0'
    s.summary = 'OriientLocationService allows to actively monitor the user location.'
    s.homepage = 'http://www.oriient.me'

    s.author = { 'Name' => 'dev@oriient.me' }
    s.license = { :type => 'Confidential', :file => 'LICENSE' }

    s.platform = :ios
    s.source = { :http => 'https://cocoapods.oriient.me/OriientLocationService/0.2.0.zip' }

    s.ios.deployment_target = '12.0'
    s.dependency 'OriientCore', '0.2.0'
    s.dependency 'LightObservables', '0.2.0'
    s.ios.vendored_frameworks = 'OriientLocationService.xcframework'
    s.swift_version = '5.0'
end
