Pod::Spec.new do |s|
    s.name = 'OriientWorldMapKit'
    s.version = '0.6.0'
    s.summary = 'With the Oriient World Map SDK you can add maps based on various map providers (Google, Apple, ESRI) to your application.'
    s.homepage = 'https://www.oriient.me'

    s.author = { 'Name' => 'dev@oriient.me' }
    s.license = { :type => 'Confidential', :file => 'LICENSE' }

    s.platform = :ios
    s.source = { :http => 'https://cocoapods.oriient.me/OriientWorldMapKit/0.6.0.zip' }

    s.ios.deployment_target = '13.0'
    s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    s.dependency 'OriientCombine', '0.1.0'
    s.ios.vendored_frameworks = 'OriientWorldMapKit.xcframework'
    s.swift_version = '5.0'
end
