Pod::Spec.new do |s|
    s.name = 'OriientRemoteLocationsProvider'
    s.version = '0.1.0'
    s.summary = 'OriientRemoteLocationsProvider provides access to the information about the location of the other users in the system.'
    s.homepage = 'http://www.oriient.me'

    s.author = { 'Name' => 'dev@oriient.me' }
    s.license = { :type => 'Confidential', :file => 'LICENSE' }

    s.platform = :ios
    s.source = { :http => 'https://cocoapods.oriient.me/OriientRemoteLocationsProvider/0.1.0.zip' }

    s.ios.deployment_target = '12.0'
    s.dependency 'OriientCore', '0.1.0'
    s.dependency 'LightObservables', '0.1.0'
    s.ios.vendored_frameworks = 'OriientRemoteLocationsProvider.xcframework'
    s.swift_version = '5.0'
end
