Pod::Spec.new do |s|
    s.name = 'OriientAppleMapProvider'
    s.version = '0.1.0'
    s.summary = 'Oriient Apple Map Provider represents a map provider for Oriient World Map which uses Apple MapKit for the map visualization.'
    s.homepage = 'https://www.oriient.me'

    s.author = { 'Name' => 'dev@oriient.me' }
    s.license = { :type => 'Confidential', :file => 'LICENSE' }

    s.platform = :ios
    s.source = { :http => 'https://cocoapods.oriient.me/OriientAppleMapProvider/0.1.0.zip' }

    s.ios.deployment_target = '12.0'
    s.dependency 'LightObservables', '0.1.0'
    s.dependency 'OriientWorldMapKit', '0.1.0'
    s.ios.vendored_frameworks = 'OriientAppleMapProvider.xcframework'
    s.swift_version = '5.0'
end
