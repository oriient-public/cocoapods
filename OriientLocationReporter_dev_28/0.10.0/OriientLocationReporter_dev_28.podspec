Pod::Spec.new do |s|
    s.name = 'OriientLocationReporter_dev_28'
    s.version = '0.10.0'
    s.summary = 'OriientLocationReporter manages the reporting of the current user location updates to the remote server.'
    s.homepage = 'https://www.oriient.me'

    s.author = { 'Name' => 'dev@oriient.me' }
    s.license = { :type => 'Confidential', :file => 'LICENSE' }

    s.platform = :ios
    s.source = { :http => 'https://cocoapods.oriient.me/OriientLocationReporter_dev_28/0.10.0.zip' }

    s.ios.deployment_target = '13.0'
    s.dependency 'OriientCore_dev_28', '0.10.0'
    s.dependency 'OriientLocationService_dev_28', '0.10.0'
    s.dependency 'OriientCombine', '0.2.0'
    s.ios.vendored_frameworks = 'OriientLocationReporter.xcframework'
    s.swift_version = '5.0'
end
