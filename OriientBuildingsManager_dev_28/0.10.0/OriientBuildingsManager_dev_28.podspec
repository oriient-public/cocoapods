Pod::Spec.new do |s|
    s.name = 'OriientBuildingsManager_dev_28'
    s.version = '0.10.0'
    s.summary = 'Buildings Manager provides access to the buildings available for the indoor positioning in the system.'
    s.homepage = 'https://www.oriient.me'

    s.author = { 'Name' => 'dev@oriient.me' }
    s.license = { :type => 'Confidential', :file => 'LICENSE' }

    s.platform = :ios
    s.source = { :http => 'https://cocoapods.oriient.me/OriientBuildingsManager_dev_28/0.10.0.zip' }

    s.ios.deployment_target = '13.0'
    s.dependency 'OriientCore_dev_28', '0.10.0'
    s.dependency 'OriientCombine', '0.2.0'
    s.ios.vendored_frameworks = 'OriientBuildingsManager.xcframework'
    s.swift_version = '5.0'
end
