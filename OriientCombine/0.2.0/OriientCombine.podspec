Pod::Spec.new do |s|
    s.name = 'OriientCombine'
    s.version = '0.2.0'
    s.summary = 'APIs and helpers related to the Combine framework'
    s.homepage = 'https://www.oriient.me'

    s.author = { 'Name' => 'dev@oriient.me' }
    s.license = { :type => 'Confidential', :file => 'LICENSE' }

    s.platform = :ios
    s.source = { :http => 'https://cocoapods.oriient.me/OriientCombine/0.2.0.zip' }

    s.ios.deployment_target = '13.0'

    s.ios.vendored_frameworks = 'OriientCombine.xcframework'
    s.swift_version = '5.0'
end
