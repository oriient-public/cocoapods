Pod::Spec.new do |s|
    s.name = 'IPSTracking'
    s.version = '2.15.0'
    s.summary = 'Oriient Indoor Tracking framework'
    s.homepage = 'http://www.oriient.me'

    s.author = { 'Name' => 'dev@oriient.me' }
    s.license = { :type => 'Confidential', :file => 'LICENSE' }

    s.platform = :ios
    s.source = { :http => 'https://cocoapods.oriient.me/IPSTracking/2.15.0.zip' }

    s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    s.ios.deployment_target = '11.0'
    s.dependency 'IPSRemoteConfig', '2.1.0'
    s.ios.vendored_frameworks = 'IPSTracking.framework'
end
