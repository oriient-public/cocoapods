Pod::Spec.new do |s|
    s.name = 'IPSRemoteConfig'
    s.version = '1.5'
    s.summary = 'Oriient Remote Config framework'
    s.homepage = 'http://www.oriient.me'
    s.author = { 'Name' => 'dev@oriient.me' }
    s.license = { :type => 'Confidential', :file => 'LICENSE' }
    s.platform = :ios
    s.source = { :http => 'https://cocoapods.oriient.me/IPSRemoteConfig/1.5.zip' }
    s.ios.deployment_target = '9.0'
    s.ios.vendored_frameworks = 'IPSRemoteConfig.framework'
end
