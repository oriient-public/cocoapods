Pod::Spec.new do |s|
    s.name = 'OriientCore'
    s.version = '0.5.0'
    s.summary = 'OriientCore manages the Oriient Location Services SDK and provides the access to its modules.'
    s.homepage = 'https://www.oriient.me'

    s.author = { 'Name' => 'dev@oriient.me' }
    s.license = { :type => 'Confidential', :file => 'LICENSE' }

    s.platform = :ios
    s.source = { :http => 'https://cocoapods.oriient.me/OriientCore/0.5.0.zip' }

    s.ios.deployment_target = '13.0'
    s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    s.dependency 'OriientCombine', '0.1.0'
    s.ios.vendored_frameworks = 'OriientCore.xcframework'
    s.swift_version = '5.0'
end
