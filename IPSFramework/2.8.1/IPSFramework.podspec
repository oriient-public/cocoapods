Pod::Spec.new do |s|
    s.name = 'IPSFramework'
    s.version = '2.8.1'
    s.summary = 'Oriient indoor positioning framework'
    s.homepage = 'http://www.oriient.me'

    s.author = { 'Name' => 'dev@oriient.me' }
    s.license = { :type => 'Confidential', :file => 'LICENSE' }

    s.platform = :ios
    s.source = { :http => 'https://cocoapods.oriient.me/IPSFramework/2.8.1.zip' }

    s.ios.deployment_target = '9.0'
    s.ios.vendored_frameworks = 'IPSFramework.framework'
end
