Pod::Spec.new do |s|
    s.name = 'IPSFramework_dev_1'
    s.version = '3.8.0'
    s.summary = 'Oriient indoor positioning framework'
    s.homepage = 'https://www.oriient.me'

    s.author = { 'Name' => 'dev@oriient.me' }
    s.license = { :type => 'Confidential', :file => 'LICENSE' }

    s.platform = :ios
    s.source = { :http => 'https://cocoapods.oriient.me/IPSFramework_dev_1/3.8.0.zip' }

    s.ios.deployment_target = '13.0'
    s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }

    s.ios.vendored_frameworks = 'IPSFramework.xcframework'
    s.swift_version = '5.0'
end
