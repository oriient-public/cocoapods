Pod::Spec.new do |s|
    s.name = 'NextNavAltitudeProvider_dev_1515'
    s.version = '0.10.0'
    s.summary = 'NextNav Altitude Provider module wraps NextNavSDK to provide enhanced altitude information for the users.'
    s.homepage = 'https://www.oriient.me'

    s.author = { 'Name' => 'dev@oriient.me' }
    s.license = { :type => 'Confidential', :file => 'LICENSE' }

    s.platform = :ios
    s.source = { :http => 'https://cocoapods.oriient.me/NextNavAltitudeProvider_dev_1515/0.10.0.zip' }

    s.ios.deployment_target = '13.0'
    s.dependency 'OriientCore_dev_1515', '0.10.0'
    s.dependency 'OriientLocationService_dev_1515', '0.10.0'
    s.dependency 'OriientCombine', '0.2.0'
    s.dependency 'NextNav', '1.0.0'

    s.ios.vendored_frameworks = 'NextNavAltitudeProvider.xcframework'
    s.swift_version = '5.0'
end
