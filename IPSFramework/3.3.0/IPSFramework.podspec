Pod::Spec.new do |s|
    s.name = 'IPSFramework'
    s.version = '3.3.0'
    s.summary = 'Oriient indoor positioning framework'
    s.homepage = 'http://www.oriient.me'

    s.author = { 'Name' => 'dev@oriient.me' }
    s.license = { :type => 'Confidential', :file => 'LICENSE' }

    s.platform = :ios
    s.source = { :http => 'https://cocoapods.oriient.me/IPSFramework/3.3.0.zip' }

    s.ios.deployment_target = '11.0'
    s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    s.dependency 'IPSRemoteConfig', '2.2.0'
    s.ios.vendored_frameworks = 'IPSFramework.xcframework'
    s.swift_version = '5.0'
end
