Pod::Spec.new do |s|
    s.name = 'OriientLocationService'
    s.version = '0.10.0'
    s.summary = 'OriientLocationService allows to actively monitor the user location.'
    s.homepage = 'https://www.oriient.me'

    s.author = { 'Name' => 'dev@oriient.me' }
    s.license = { :type => 'Confidential', :file => 'LICENSE' }

    s.platform = :ios
    s.source = { :http => 'https://cocoapods.oriient.me/OriientLocationService/0.10.0.zip' }

    s.ios.deployment_target = '13.0'
    s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    s.dependency 'OriientCore', '0.10.0'
    s.dependency 'OriientCombine', '0.1.0'
    s.ios.vendored_frameworks = 'OriientLocationService.xcframework'
    s.swift_version = '5.0'
end
