Pod::Spec.new do |s|
    s.name = 'IndoorMapSDK'
    s.version = '8.0.0'
    s.summary = 'IndoorMapSDK provides a basic toolkit to present an interactive indoor map to a user'
    s.homepage = 'https://www.oriient.me'

    s.author = { 'Name' => 'dev@oriient.me' }
    s.license = { :type => 'Confidential', :file => 'IndoorMapSDK.xcframework/LICENSE' }

    s.platform = :ios
    s.source = { :http => 'https://cocoapods.oriient.me/IndoorMapSDK/8.0.0.zip' }

    s.ios.deployment_target = '13.0'

    s.dependency 'IPSFramework', '8.0.0'

    s.ios.vendored_frameworks = 'IndoorMapSDK.xcframework'
    s.swift_version = '5.0'
end
