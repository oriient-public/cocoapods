Pod::Spec.new do |s|
    s.name = 'OriientESRIMapProvider'
    s.version = '0.6.1'
    s.summary = 'Oriient ESRI Map Provider represents a map provider for Oriient World Map which uses ESRI ArcGIS iOS SDK for the map visualization.'
    s.homepage = 'https://www.oriient.me'

    s.author = { 'Name' => 'dev@oriient.me' }
    s.license = { :type => 'Confidential', :file => 'LICENSE' }

    s.platform = :ios
    s.source = { :http => 'https://cocoapods.oriient.me/OriientESRIMapProvider/0.6.1.zip' }

    s.ios.deployment_target = '13.0'
    s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    s.dependency 'OriientCombine', '0.1.0'
    s.dependency 'OriientWorldMapKit', '0.6.1'
    s.dependency 'ArcGIS-Runtime-SDK-iOS', '100.10'

    s.ios.vendored_frameworks = 'OriientESRIMapProvider.xcframework'
    s.swift_version = '5.0'
end
