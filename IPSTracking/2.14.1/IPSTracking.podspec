Pod::Spec.new do |s|
    s.name = 'IPSTracking'
    s.version = '2.14.1'
    s.summary = 'Oriient Indoor Tracking framework'
    s.homepage = 'http://www.oriient.me'

    s.author = { 'Name' => 'dev@oriient.me' }
    s.license = { :type => 'Confidential', :file => 'LICENSE' }

    s.platform = :ios
    s.source = { :http => 'https://cocoapods.oriient.me/IPSTracking/2.14.1.zip' }

    s.ios.deployment_target = '9.0'
    s.dependency 'IPSRemoteConfig', '1.6'
    s.ios.vendored_frameworks = 'IPSTracking.framework'
end
