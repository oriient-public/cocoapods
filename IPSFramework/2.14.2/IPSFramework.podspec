Pod::Spec.new do |s|
    s.name = 'IPSFramework'
    s.version = '2.14.2'
    s.summary = 'Oriient indoor positioning framework'
    s.homepage = 'http://www.oriient.me'

    s.author = { 'Name' => 'dev@oriient.me' }
    s.license = { :type => 'Confidential', :file => 'LICENSE' }

    s.platform = :ios
    s.source = { :http => 'https://cocoapods.oriient.me/IPSFramework/2.14.2.zip' }

    s.ios.deployment_target = '9.0'
    s.dependency 'IPSRemoteConfig'
    s.ios.vendored_frameworks = 'IPSFramework.framework'
end
