Pod::Spec.new do |s|
    s.name = 'OriientCore_dev_21'
    s.version = '0.10.0'
    s.summary = 'OriientCore manages the Oriient Location Services SDK and provides the access to its modules.'
    s.homepage = 'https://www.oriient.me'

    s.author = { 'Name' => 'dev@oriient.me' }
    s.license = { :type => 'Confidential', :file => 'LICENSE' }

    s.platform = :ios
    s.source = { :http => 'https://cocoapods.oriient.me/OriientCore_dev_21/0.10.0.zip' }

    s.ios.deployment_target = '13.0'
    s.dependency 'OriientCombine', '0.2.0'
    s.ios.vendored_frameworks = 'OriientCore.xcframework'
    s.swift_version = '5.0'
end
