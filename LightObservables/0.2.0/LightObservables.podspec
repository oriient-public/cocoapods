Pod::Spec.new do |s|
    s.name = 'LightObservables'
    s.version = '0.2.0'
    s.summary = 'A simple and lightweight Observable pattern implementation with no third-party dependencies.'
    s.homepage = 'https://www.oriient.me'

    s.author = { 'Name' => 'dev@oriient.me' }
    s.license = { :type => 'Confidential', :file => 'LICENSE' }

    s.platform = :ios
    s.source = { :http => 'https://cocoapods.oriient.me/LightObservables/0.2.0.zip' }

    s.ios.deployment_target = '12.0'
    s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }

    s.ios.vendored_frameworks = 'LightObservables.xcframework'
    s.swift_version = '5.0'
end
