Pod::Spec.new do |s|
    s.name = 'OriientGoogleMapProvider'
    s.version = '0.2.0'
    s.summary = 'Oriient Google Map Provider represents a map provider for Oriient World Map which uses Google Maps iOS SDK for the map visualization.'
    s.homepage = 'https://www.oriient.me'

    s.author = { 'Name' => 'dev@oriient.me' }
    s.license = { :type => 'Confidential', :file => 'LICENSE' }

    s.platform = :ios
    s.source = { :http => 'https://cocoapods.oriient.me/OriientGoogleMapProvider/0.2.0.zip' }

    s.ios.deployment_target = '12.0'
    s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    s.dependency 'LightObservables', '0.2.0'
    s.dependency 'OriientWorldMapKit', '0.2.0'
    s.dependency 'GoogleMaps', '4.2.0'
    s.ios.vendored_frameworks = 'OriientGoogleMapProvider.xcframework'
    s.swift_version = '5.0'
end
